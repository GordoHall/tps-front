import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { PriceTableComponent } from './price-table/price-table.component';
import { BookingComponent } from './booking/booking.component';
import { NgxChartsModule} from '@swimlane/ngx-charts';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LivePricingComponent } from './live-pricing/live-pricing.component';

@NgModule({
  declarations: [
    AppComponent,
    PriceTableComponent,
    BookingComponent,
    LivePricingComponent,
  ],

  imports: [
    BrowserModule, 
    HttpClientModule, 
    FormsModule,
    NgxChartsModule,
    NoopAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
