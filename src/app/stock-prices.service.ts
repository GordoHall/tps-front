import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StockPricesService {

  constructor(private  httpClient:HttpClient) { }

  public getPrice(ticker: string, days): Observable<any>{
    return this.httpClient.get<any>(environment.priceUrl+"?ticker="+ticker+"&num_days="+days);
    //return this.httpClient.get(tes)
  }

  public getTopStocks(): Observable<any>{
    return this.httpClient.get(environment.testUrl + "topStocks/");
  };
  public getSP(): Observable<any>{
    return this.httpClient.get(environment.testUrl + "markets/");
  };
  public getAdvice(ticker: string): Observable<any> {
    return this.httpClient.get(
      environment.testUrl + "advice/" + ticker
    )
  }
}
