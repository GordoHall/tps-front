import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BookingServiceService {

  constructor(private httpClient: HttpClient) { }

  public getAllTrades(): Observable<any>{
    return this.httpClient.get<any>(environment.tradesUrl);
  }
  public getTrade(id): Observable<any>{
    return this.httpClient.get<any>(environment.tradesUrl+"/"+id);
  }
  public postTrade(json): Observable<any>{
    return this.httpClient.post<any>(environment.tradesUrl+"/",json,{headers : new HttpHeaders({ 'Content-Type': 'application/json' })});
  }
}
