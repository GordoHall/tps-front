import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StockPricesService } from '../stock-prices.service';

@Component({
  selector: 'app-price-table',
  templateUrl: './price-table.component.html',
  styleUrls: ['./price-table.component.css']
})
export class PriceTableComponent implements OnInit {

  public priceData:any;
  public ticker: string;
  public days:number;
  public chartData: any;

  // chart options
  //options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  yAxisLabel: string = 'USD';
  timeline: boolean = true;

  constructor(private stockPricesService: StockPricesService) { }

  ngOnInit(): void {
    this.stockPricesService.getPrice("AAPL",5).subscribe( (data) => {
        console.log(data);
        this.priceData = data;
    });
  }

  get(): void {
    this.stockPricesService.getPrice(this.ticker,this.days).subscribe( (data) => {
      console.log(data);  
      this.priceData = data;
      this.createChartData();
      
    }, error => {
      console.log("Error reading  price data:");
      console.log(error);
    }
  )
}

  createChartData() {
    this.chartData = { name: this.ticker, series: [] };

    for (let price of this.priceData) {
      this.chartData.series.push({
        name: price[0],
        value: price[1]
      });
    }

    this.chartData = [this.chartData];
  }

}