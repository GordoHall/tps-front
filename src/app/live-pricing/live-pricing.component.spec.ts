import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivePricingComponent } from './live-pricing.component';

describe('LivePricingComponent', () => {
  let component: LivePricingComponent;
  let fixture: ComponentFixture<LivePricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivePricingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivePricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
