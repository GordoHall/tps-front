import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StockPricesService } from '../stock-prices.service';

@Component({
  selector: 'app-live-pricing',
  templateUrl: './live-pricing.component.html',
  styleUrls: ['./live-pricing.component.css']
})
export class LivePricingComponent implements OnInit {
 

  constructor(private httpClient: HttpClient, private stockService: StockPricesService) { }

  stocks: any;
  spStocks: any;
  ticker: string;
  adviceData: any;

  ngOnInit(): void {
    this.stockService.getTopStocks().subscribe(
      responseData => {
        console.log("Recieved Response Data:");
        console.log(responseData);
        this.stocks = responseData;
       
      },
      error => {
        console.log("Error retreiving employee list");
        console.log(error);
      }
    );
    this.stockService.getSP().subscribe(
      responseData => {
        console.log("Recieved Response Data:");
        console.log(responseData);
        this.spStocks = responseData;
       
      },
      error => {
        console.log("Error retreiving employee list");
        console.log(error);
      }
    );
    
  }
  getPriceData(){
    this.stockService.getAdvice(this.ticker).subscribe(
      responseData => {
        console.log("Recieved Advice Data:");
        console.log(responseData);
        this.adviceData = responseData;
        
      
      },
      error => {
        console.log("Error retreiving employee list");
        console.log(error);
      }
    );
  }
}
