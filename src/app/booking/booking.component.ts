import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';

import { BookingServiceService } from '../booking-service.service';
import { StockPricesService } from '../stock-prices.service';
import { Observable, timer, Subscription } from 'rxjs';
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  public tradeData;
  public updateSubscription: Subscription;
  public stock;
  public price;
  public qty;
  public type;
  public trade = {
    dateCreated: "",
    reqPrice: 0,
    state: null,
    stockQuantity: 0,
    stockTicker: "",
    tradeType: ""
  }

  constructor(private bookingService:  BookingServiceService, private priceService:StockPricesService) { }

  ngOnInit() {
    this.updateTrades();
  }
  public bookTrade(){
    this.priceService.getPrice(this.stock,1).subscribe(response=>{
      this.price = response.price_data[0][1];
      this.trade.dateCreated =new Date().toLocaleTimeString()+" : " +new Date().toLocaleDateString()
      this.trade.reqPrice=this.price;
      this.trade.state= "CREATED";
      this.trade.stockQuantity = this.qty;
      this.trade.stockTicker = this.stock;
      this.trade.tradeType = this.type
      if (this.type == 'BUY' && this.qty > 0){
        confirm("Are you sure you wish to purchase "+this.qty+" of "+this.stock+" stock for $"+this.price+" a share for a total of $"+(this.qty*this.price)+"?");
        this.bookingService.postTrade(JSON.stringify(this.trade)).subscribe(data => console.log(data))
      } else if (this.type == 'SELL' && this.qty > 0){
        confirm("Are you sure you wish to sell " + this.qty + " of " + this.stock + " stock for $" + this.price +" a share for a total of $" + (this.qty * this.price) + "?")
        this.bookingService.postTrade(JSON.stringify(this.trade)).subscribe(data => console.log(data))
      } else {
        alert("Please select either BUY or SELL or add amount");
      }
      
      console.log(this.trade);
      
    });
   
  }
  private updateTrades() {
    this.bookingService.getAllTrades().subscribe(repsonseData => {this.tradeData=repsonseData;
      console.log(this.tradeData)
        this.startRefreshTimer();
      },error => {console.log(error)});
    }
  
    private startRefreshTimer() {
      this.updateSubscription = timer(10000).subscribe(
        (val) => { this.updateTrades() }
      );
    }
  
}
